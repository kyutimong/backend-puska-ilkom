from flask import Flask, Blueprint

import application.database

from application.user.login import login

app = Flask(__name__)

app.register_blueprint(login,url_prefix='/login')

if __name__ == '__main__':
    app.run(debug=True)
