from flask import Flask, request, jsonify, make_response, Blueprint
from flask_jwt import JWT, jwt_required, current_identity
from application.database import select, insert
import hashlib

user = Blueprint('user', __name__,
                 static_folder='../../upload/user', static_url_path="/media")

@user.route('/insert_data_user', methods=['POST'])
def insert_data_user():

    hasil = {"status": "gagal insert data user"}

    try:
        data = request.json
        pass_ency = hashlib.md5(data["password"].encode('utf-8')).hexdigest()
        query = "INSERT INTO tb_user (email,username,no_telepon,password) VALUES(%s,%s,%s,%s)"
        values = (data["email"], data["username"],
                  data["no_telepon"], pass_ency,)
        insert(query, values)
        hasil = {"status": "berhasil insert data user"}
    except Exception as e:
        print("Error: " + str(e))
    return jsonify(hasil)
