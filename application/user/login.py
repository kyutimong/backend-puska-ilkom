from flask import Flask, request, jsonify, make_response, Blueprint
from hashlib import md5
import json
import pymysql

import application.database as database

login = Blueprint('login', __name__)

def check(email):
    qry = "select * from users where email = %s"
    val = (email)
    database.db.execute(qry, val)
    hasil = database.db.fetchone()
    if hasil != None:
        return True
    else:
        return False

@login.route('/', methods=['POST'])
def login_account():
    data = request.json
    if check(data['email']):
        pwd = md5(data['password'].encode('utf-8')).hexdigest()
        qry = "select * from users where email = %s and password = %s"
        val = (data['email'], pwd)
        database.db.execute(qry, val)
        hasil = database.db.fetchone()
        if hasil != None:
            state = {'Status':'Berhasil login'}
            return jsonify(state)
        else:
            state = {'Status':'Password salah'}
            return jsonify(state)
    else:
        state = {'Status':'Email/Username belum terdaftar'}
        return jsonify(state)
